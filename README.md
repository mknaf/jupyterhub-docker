# About

A `Dockerfile` based on `ubuntu:18.04` that creates an image
providing a multi-user development environment for Data Scientists.

Maintainer: [Michael Knaf](mailto:michael.knaf@sovanta.com)

# Table of Contents
<!-- TOC -->

- [About](#about)
- [Table of Contents](#table-of-contents)
- [Requirements](#requirements)
- [Quickstart](#quickstart)
  - [Python Packages](#python-packages)
- [Contributing](#contributing)
  - [commitlint](#commitlint)
  - [Commitizen](#commitizen)
- [Setting up the Container](#setting-up-the-container)
  - [Users](#users)
    - [userlist.txt](#userlisttxt)
    - [root](#root)
  - [File and Directory Mapping](#file-and-directory-mapping)
    - [JupyterHub Config](#jupyterhub-config)
    - [User Homes](#user-homes)
    - [Shared Directory](#shared-directory)
  - [Port Mapping](#port-mapping)
- [Services](#services)
  - [JupyterHub](#jupyterhub)
    - [JupyterLab](#jupyterlab)
  - [SSH](#ssh)
    - [rsync](#rsync)
    - [SFTP & VSCode](#sftp--vscode)
  - [cron](#cron)
    - [Backups](#backups)
    - [rdp](#rdp)
- [Environment](#environment)
  - [Environment Variables](#environment-variables)
    - [JupyterHub](#jupyterhub-1)
    - [SSH Server](#ssh-server)
  - [Command Line](#command-line)
    - [tmux](#tmux)
    - [zsh Plugins](#zsh-plugins)
    - [~~vim~~ neovim](#vim-neovim)
    - [Other](#other)
      - [node](#node)
      - [Python](#python)
      - [Random Tools](#random-tools)
  - [UI](#ui)
    - [cairo-dock](#cairo-dock)
- [User Software or Tool Requirements](#user-software-or-tool-requirements)
- [Proxies](#proxies)
- [Known Issues](#known-issues)
  - [Missing `man` Pages](#missing-man-pages)
- [References](#references)

<!-- /TOC -->

# Requirements

- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)

# Quickstart

- Install [requirements](#requirements).
- Make sure to have a user that is allowed to use docker.
- Add [users](#users) and their [passwords](#userlisttxt) in `app/userlist.txt`.
- Build and run with `docker-compose up --build --detach`.

## Python Packages

Packages are being installed at container build-time. They're read from
[`src/app/requirements.txt`](src/app/requirements.txt).
Please pin packages to precise versions,
otherwise we're prone for dependency hell (again).

# Contributing

First-time setup is just running `npm install`.

The project uses [husky](https://github.com/typicode/husky) to
work with commit hooks. Commit hooks will be enabled when installing `husky`
via `npm install`. If all goes as planned, you should see something like this
during the run of `npm install`:

```txt
husky > setting up git hooks
husky > done
```

Hooks are defined in [.huskyrc](.huskyrc).

## commitlint

There is a `commit-msg` hook upon which [`husky`](#Husky) will check that
the formatting policy for the commit message defined by the
[Angular Git Commit Guidelines](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines)
has been fulfilled.

## Commitizen

Contributors are encouraged to use
[commitizen](https://github.com/commitizen/cz-cli) when authoring
commit messages.

In order to use `commitizen`'s features, stage your changes as usual.
Then, instead of running `git commit`, use `npm run gcm` or `git cz`
to start the interactive prompt on the command line.

> GUI users will have to author compliant messages by themselves.

# Setting up the Container

## Users

It is only possible to use `JupyterHub` with system users. Therefore, we
need to create the system users on container start. This happens before
we start JupyterHub, in [`src/app/run`](src/app/run),
which is the `CMD` for the container,
i.e. which is run when the container starts.

### userlist.txt

To be able to determine which users we need to create, `src/app/run` looks
for a file [`/app/userlist.txt`](app/userlist.txt),
which we mount into the container. That way
we can manipulate the list for each container if need be.

`src/app/run` iterates each line in `userlist.txt` and parses a username and an
_encrypted_ password from the current line. It then creates that user using
`adduser` and, after that is done, injects the encrypted password into
`/etc/shadow`.

To create the encrypted password, I've been using

```bash
openssl passwd -1 -salt <yoursalt> [yourpass]
```

If you don't pass `yourpass` to the command it will ask for it interactively.

> Please make sure to have an empty line at the end of the file,
otherwise parsing of the lines can fail if there is only 1 user listed,
which would then mean that no user is being created.

### root

It is not possible to become `root` from inside the container itself.
The `root` user is blacklisted in JupyterHub, and not allowed to log in
to `SSH`. There is no `sudo` installed, and in fact the `root` user doesn't
even have a password assigned.

The only way to become `root` is by executing the `connect_bash` script
in this repository. This magically opens a `bash` inside the container via the
docker server.

## File and Directory Mapping

The files in [`app`](app/) are [bind-mounts](https://docs.docker.com/storage/bind-mounts/).

The files in [`src`](src/) are being copied at build-time into the container.

> By convention, the `src` dir should reflect the root `/` of the
container.

### JupyterHub Config

[`app/jupyterhub_config.py`](app/jupyterhub_config.py) will be bind-mount into the container.

`JupyterHub` administrators need to be listed in that file.

The config also contains a white- and a blacklist. `root` is blacklisted. If the
whitelist is empty, all system users are allowed to log in, which is how
it's meant to be. Simply only create the users you need in `userlist.txt`
and no one else will be able to log in.

### User Homes

The `/home` directory is a docker [volume](https://docs.docker.com/storage/volumes/).
Upon container start, the `/app/run`
script will attempt to create users and user homes. If they already exist,
nothing will be overwritten.

### Shared Directory

There's a shared directory at `/share` that is also a docker volume.
Every user should have a link to
it in his home which is created at run-time. Also, every user should have
a directory inside `/share` with his name.

We use `acl`s to automatically have files and directories created in `/share`
being owned by the group `ds` (in which every user is being put at run-time).

This means that every user can:

- Read everything anywhere in `/share/**/*`
- Write to his own dir in `/share/<username>`
- Read, write, _and delete_ everything in `/share/common`

Due to the nature of `acl`s, only files that are being _newly created_
in the directory get the permissions applied correctly. Files that
are being _copied_ into that directory are going to keep their previous
ownership and permissions, and might therefore not be writable by other
users. In order to fix the permissions for all files in `/share/common`,
you need to run `setfacl --recursive --modify="d:g:ds:rwx,g:ds:rwx" /share/common`
as `root` in the container. This is also what happens on container start, see
[`src/app/run/`](src/app/run#L29).

## Port Mapping

In order to access the services running in the container, some ports need
to be mapped at runtime. I understand that ports from inside the container
are being mapped to ports on the host machine.

This means that if we're
a user on the _host_ system, we could for instance access the container's
`JuptyerHub` in the _host_ browser at `localhost:8000`,
given that the ports are mapped like that.

From another machine the same holds true with the remote's
address like `some.remote.server.com:8000`, except that this doesn't even
require a host system user.

The same is true for `SSH`, although depending on your ssh client you might
have to pass the port argument: `ssh -p 8022 localhost`.

# Services

This section contains info on the services running in the container.
All services are being kept alive by [supervisord](http://supervisord.org/).

`supervisord`'s control interface is reachable via [`supervisorctl`](http://supervisord.org/running.html#running-supervisorctl).
While all users might be able to start the interface, only the `root` user has
the permissions to actually control the system.

The config for the `supervisor` is being loaded from [`/etc/supervisord.conf`](src/etc/supervisord.conf)
by the `/app/run` script. `supervisor` then loads the config files for each
service it is supposed to run from [`/etc/supervisor/conf.d`](src/etc/supervisor/conf.d).

## JupyterHub

Having a stable [`JupyterHub`](https://github.com/jupyterhub/jupyterhub)
instance was the original idea for this container. Inside the container
it runs on port 8000. Depending on the [`docker-compose.yml`](docker-compose.yml),
this might be mapped to a different port on the host.

`JupyterHub` is only a proxy for the actual `Jupyter` so we can
enable multiple users running their notebooks separately. To be precise,
we run [`JupyterLab`](https://github.com/jupyterlab/jupyterlab) as
the single-user instance.

### JupyterLab

There are [tons of plugins for JupyterLab](https://github.com/topics/jupyterlab-extension)
around. Currently, these are installed:

- [Statusbar](https://github.com/jupyterlab/jupyterlab-statusbar)

## SSH

An SSH server is running on the default port 22 inside the container.
Let's say the port is mapped to port 8022 on the host, you can connect
to it by calling `ssh://<yourname>@localhost:8022`, where `<yourname>`
must have been defined in [`userlist.txt`](#userlisttxt).

### rsync

`rsync` is installed on the machine. If the `SSH` port is not mapped
to the default port 22, you need to tell `rsync` to use a different port.
In this case, we need to notify the `ssh` client used internally by `rsync`
of the port like this:

```bash
rsync -e 'ssh -p 8022' <src_path> <user>@localhost:<trg_path>
```

### SFTP & VSCode

It is possible to connect to the jhub container via SFTP
from VSCode versions 1.23.0 or higher.

1. Download VSCode `.zip` (!) file for your architecture from
   [here](https://code.visualstudio.com/download).
1. Enable portable mode for VSCode as described
   [here](https://code.visualstudio.com/docs/editor/portable).
1. Install [vscode-sftp](https://github.com/liximomo/vscode-sftp), preferably
   via the VSCode plugin manager.
1. Configure the vscode-sftp plugin as described on the plugin's
   [homepage](https://github.com/liximomo/vscode-sftp#usage).

## cron

There is a cron service running in the container.
Initially the service was just meant to take care of regular backups
(which it does), but normal users can also add
and run jobs via the default `crontab -e`.

### Backups

A cron job does regular backups using [borg](https://borgbackup.readthedocs.io),
which we access through [borgmatic](https://torsion.org/borgmatic/).
See `src/etc/borgmatic/config.yaml` for details on the backup and pruning strategies.

The cron job is described in `src/etc/cron.d/borgmatic` and currently runs
every full hour.

_NOTE: The backup hasn't been tested yet!_

### rdp

There is an [xrdp](http://www.xrdp.org/) server running. Several clients are
available, see the homepage for details. The default Microsoft RDP Client
is also supposed to work out of the box, even the one for OSX.

# Environment

## Environment Variables
Depending on the project, there might be some environment variables
[set via docker](https://docs.docker.com/docker-cloud/getting-started/deploy-app/6_define_environment_variables/).

> By convention, the variable names should start with a `SOV_DS_` prefix.

### JupyterHub
`JupyterHub` by default does _not_ pass all environment variables to the
`Jupyter`(`Lab`) instances that it spawns. To make it pass variables that
we want, they need to be explicitly whitelisted. This happens dynamically
when the `JupyterHub` service starts, see [the config](app/jupyterhub_config.py#L540).

### SSH Server
Similar to `JupyterHub`, `openssh-server` does not pass all arguments to
the shells it opens for logged-in users. This is solved by dynamically
adding variables starting with `SOV_DS_` to `/etc/bash.bashrc` when
the server comes up, where `bash` will read them when starting. Therefore,
you should not log in with a shell different from `bash`.

## Command Line

The default command line environment is a standard `/bin/bash`, with
the addition of the scripts in [`/etc/profile.d](src/etc/profile.d).

A more advanced command line environment can be entered using [`tmux`](https://github.com/tmux/tmux/wiki).
It uses `zsh` as the default shell and has a bunch of `zsh` plugins
installed. _It is recommended to use this environment instead of
the default, and [here](https://leanpub.com/the-tao-of-tmux/read)'s why._

### tmux

The default config for `tmux` is being loaded from [`/etc/tmux.conf`](src/etc/tmux.conf).
I suppose that it is possible to override these settings with a `~/.tmux.conf` file.

### zsh Plugins

Note: _There might be
warnings during the first start of `zsh`. These can be ignored and
are supposed to not show up anymore after the first run of the shell._

Plugins for `zsh` are installed via [`antibody`](http://getantibody.github.io/).
They are collected in [`/etc/zsh/antibody-plugins.txt`](src/etc/zsh/antibody-plugins.txt)
and will be installed for each user in their home directory
when first starting `zsh`.

There are more plugins in [`/etc/zsh/plugins/`](src/etc/zsh/plugins)
that aren't being installed
individually for each user by `antibody` but are instead loaded through
the default [`/etc/zsh/zshrc`](src/etc/zsh/zshrc), that `zsh` tries to load when it starts.
Users could override or enhance settings with their own `~/.zshrc` as this
will be loaded later.

### ~~vim~~ neovim

`vim` (likely `vim.basic`) is installed, but with no special config.

`neovim` comes with a useful initial config and some predefined plugins
that are read from [`/usr/share/nvim/sysinit.vim`](src/usr/share/nvim/sysinit.vim).
Install plugins with `:PlugInstall`. [`vim-plug`](https://github.com/junegunn/vim-plug)
takes care of installing.

### Other

#### node

We manage global `nodejs` installation with [`n`](https://github.com/tj/n).
I chose this over [`nvm`](https://github.com/creationix/nvm) because `nvm`
is explicitly _not_ meant to be installed globally.

#### Python

We manage global `Python` installation with [`pyenv`](https://github.com/pyenv/pyenv).

#### Random Tools

There are a bunch of extra command line tools installed in the container:

- [how2](https://github.com/santinic/how2) - stackoverflow from the terminal
- [httpie](https://httpie.org/) - a better command line HTTP client
- [mycli](https://www.mycli.net/) - a better command line MySQL client (invoke with `LANG=C.UTF-8 mycli` to prevent errors)
- ~~[pageres-cli](https://github.com/sindresorhus/pageres-cli) - capture screenshots of websites~~ did make problems with npm, therefore currently not installed
- [bat](https://github.com/jupyterlab/jupyterlab-statusbar) - a `cat` replacement with syntax highlighting

## UI

### cairo-dock

For a better user experience a task bar called cairo-dock is added. It is similiar
to macOS task bar. http://glx-dock.org/


# User Software or Tool Requirements

Especially in the early days of working in the container we will encounter
missing software. Python packages, command line tools, libraries etc.
I think the best way to get a package into the system is this:

- clone this repository (for example on your MacBook)
- modify the `Dockerfile` so that it will install your desired software
- make sure the resulting container still works as expected
- commit all changes onto a separate branch
- push this branch to our internal GitLab and
- send a merge request to the Maintainer

Alternatively, you could just let the Maintainer know what you need and
he will take care of it for you.

Either way, installing a new package includes rebuilding, testing, and
redeploying the container and thus takes some time.

It is strongly _not_ recommended to install software into the container
during run-time. If you did that, keep in mind that these changes will
be gone as soon as the container instance goes down. If you still want
to install software while running the container, also make sure that
you update the Dockerfile or requirements.txt so that the package will
be installed the next time the container is built.

# Proxies

Ignore this section if you don't use a proxy.

When using Docker version `17.07` or higher, you should configure the docker client
to know about the proxy by having a file `~/.docker/config.json` containing:

```json
{
  "proxies": {
    "default": {
      "httpProxy": "http://127.0.0.1:3001",
      "noProxy": "*.test.example.com,.example2.com"
    }
  }
}
```

For versions of Docker older than `17.07`, please refer to the
[documentation](https://docs.docker.com/network/proxy/).

# Known Issues

## Missing `man` Pages

The base image is a stripped-down version of ubuntu. Therefore,
certain standard stuff isn't available, for instance `man` pages.
If we need this stuff, we need to call `unminimize` during build.
This seems to re-run all `apt-get install` commands to recreate
the full installation, so I recommend to do that early in the
`Dockerfile` to avoid package reinstallation.

# References

I did quite some reading on the whole topic, so here's some more or less
useful links:

- Other methods of password encryption: https://unix.stackexchange.com/a/81248/269571
- Format of `etc/passwd` file:
  https://www.ibm.com/support/knowledgecenter/en/ssw_aix_72/com.ibm.aix.security/passwords_etc_passwd_file.htm
- Format of `etc/shadow` file: https://www.tldp.org/LDP/lame/LAME/linux-admin-made-easy/shadow-file-formats.html
- How to manually add a user in Linux: https://www.tldp.org/LDP/sag/html/adduser.html#MANUAL-ADDUSER
- Understand how `uid` and `gid` work in Docker containers and why we have
  users inside the container instead of mapping host system users:
  https://medium.com/@mccode/understanding-how-uid-and-gid-work-in-docker-containers-c37a01d01cf
- Source of `xterm-256color-italic.terminfo`: https://gist.github.com/sos4nt/3187620
- What the `LC_ALL` environment variable actually does: https://unix.stackexchange.com/a/87763/269571
- ALL you'll ever need to know about cron jobs: https://serverfault.com/a/449652
- A helper to get your cron timings right: https://crontab.guru
- How I fixed the locale in the container: http://jaredmarkell.com/docker-and-locales/