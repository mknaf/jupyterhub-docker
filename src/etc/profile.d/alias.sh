#!/bin/sh

alias ls='ls --color'
alias mycli='LANG=C.UTF-8 mycli'
alias tm='tmux attach || tmux'