# start from a default linux image
FROM ubuntu:18.04

# make sure we're up to date
RUN apt-get update

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get -y upgrade \
    && DEBIAN_FRONTEND=noninteractive apt-get -y install \
    # mknaf recommends not allowing sudo. people will abuse it and make a mess.
    #sudo \
    acl \
    apt-utils \
    bash \
    build-essential \
    ca-certificates \
    #cairo-dock \
    cron \
    curl \
    cython \
    fzy \
    git \
    git-flow \
    graphviz \
    hdf5-tools \
    htop \
    imagemagick \
    iputils-ping \
    firefox \
    language-pack-en \
    libacl1 \
    libacl1-dev \
    libaio1 \
    libbz2-dev \
    libffi-dev \
    libmysqlclient20 \
    libncurses5-dev \
    libreadline-dev \
    libsqlite3-dev \
    libssl-dev \
    libssl-dev \
    libxml2-dev \
    libxmlsec1-dev \
    llvm \
    make \
    mongodb \
    mysql-client \
    nano \
    neovim \
    net-tools \
    openssh-client \
    openssh-server \
    pandoc \
    psmisc \
    rsync \
    silversearcher-ag \
    supervisor \
    telnet \
    terminator \ 
    tk-dev \
    tmux \
    unzip \
    vim \
    wget \
    xfce4 \
    xrdp \
    xz-utils \
    zlib1g-dev \
    zsh \
    # cleanup
    && rm -rf /var/lib/apt/lists/*

# clone n, the node version manager
RUN git clone https://github.com/tj/n /tmp/n \
    # install n
    && cd /tmp/n && make install \
    # remove the cloned sources
    && cd / && rm -rf /opt/n \
    # activate a node version (which installs it if it doesn't exist yet)
    && n 10.11.0

# install proxy required by JupyterHub
RUN npm install -g configurable-http-proxy \
    # and some more packages and tools
    how-2

# install pyenv
ENV PYENV_ROOT /opt/pyenv
RUN git clone https://github.com/yyuu/pyenv.git ${PYENV_ROOT}

# which python version do we want?
ENV PYENV_PY_VERSION 3.7.1
# make sure the pyenv shims are available to everybody through /etc/environment
RUN sed -i 's@PATH="@PATH="'"${PYENV_ROOT}"'/shims:@g' /etc/environment
# make pyenv and shims available to the docker build process
ENV PATH ${PYENV_ROOT}/bin:${PYENV_ROOT}/shims:${PATH}
# update pyenv
# actually install the desired python version
RUN pyenv install ${PYENV_PY_VERSION} \
    # set the desired version to be the globally used version
    && pyenv global ${PYENV_PY_VERSION}

# use this as the working directory
WORKDIR /app

# install required python packages
COPY src/app/requirements.txt /app/requirements.txt
RUN python3 -m pip install --upgrade pip \
    && python3 -m pip install -r /app/requirements.txt --no-cache-dir \
    # and install scikit-garden manually because it is broken via requirements.txt
    && python3 -m pip install scikit-garden \
    # and clean up afterwards
    && rm -rf ~/.cache/pip \
    # make sure pyenv knows about all these modules we just installed
    && pyenv rehash

# install extensions for JupyterLab
RUN jupyter labextension install @jupyterlab/hub-extension
RUN jupyter labextension install @jupyter-widgets/jupyterlab-manager
RUN jupyter nbextension install --py widgetsnbextension
RUN jupyter nbextension enable --py widgetsnbextension

# copy all supervisor configs into the container
COPY src/etc/supervisor/ /etc/supervisor/
COPY src/etc/supervisord.conf /etc/supervisord.conf

# sshd wants this to exist
RUN mkdir /run/sshd
# copy the config over
COPY src/etc/ssh/sshd_config /etc/ssh/sshd_config

# make sure we have the terminfo for xterm-256color-italic installed,
# in case someone logs in with this terminal type.
COPY src/app/xterm-256color-italic.terminfo /app/xterm-256color-italic.terminfo
RUN tic /app/xterm-256color-italic.terminfo

# make sure we have the terminfo for tmux-256color-italic installed for tmux
COPY src/app/tmux-256color-italic.terminfo /app/tmux-256color-italic.terminfo
RUN tic /app/tmux-256color-italic.terminfo
# install antibody, a zsh plugin manager. skip that the installer
# wants to use sudo - we're root anyways and don't want to install sudo either.
RUN curl -sL git.io/antibody | sed 's/sudo //g' | sh -s


# copy and extract oracle db driver to the container.
# do this late in Dockerfile to avoid unnecessary rebuilding.
ENV ORCL_INSTACLIENT_FNAME instantclient-basic-linux.x64-18.3.0.0.0dbru.zip
COPY src/app/${ORCL_INSTACLIENT_FNAME} /app/${ORCL_INSTACLIENT_FNAME}
RUN unzip /app/${ORCL_INSTACLIENT_FNAME} -d /app/ \
    && rm -f ${ORCL_INSTACLIENT_FNAME}
# inject oracle db libs into library config
RUN echo '/app/instantclient_18_3' >> /etc/ld.so.conf.d/instantclient.conf \
    && ldconfig

# download and install visual studio code
ENV VSCODE_DEB_NAME code_1.27.2-1536736588_amd64.deb
ENV VSCODE_DEB_URL https://az764295.vo.msecnd.net/stable/f46c4c469d6e6d8c46f268d1553c5dc4b475840f/"${VSCODE_DEB_NAME}"
ENV VSCODE_DEB_FULLNAME /app/"${VSCODE_DEB_NAME}"
RUN curl "${VSCODE_DEB_URL}" -o "${VSCODE_DEB_FULLNAME}" \
    && dpkg -i "${VSCODE_DEB_FULLNAME}" \
    && rm "${VSCODE_DEB_FULLNAME}"

# add borgmatic config file
COPY src/etc/borgmatic/config.yaml /etc/borgmatic/config.yaml
# add borgmatic backup cron task
COPY src/etc/cron.d/borgmatic /etc/cron.d/borgmatic
# set correct permissions on the cron job
RUN chmod 0644 /etc/cron.d/borgmatic

# add global tmux.conf
COPY src/etc/tmux.conf /etc/tmux.conf
RUN chmod 0644 /etc/tmux.conf

# set (or rather, fix) locales for all users
COPY src/etc/profile.d/locale.sh /etc/profile.d/locale.sh

# a global default zshrc
COPY src/etc/zsh/zshrc /etc/zsh/zshrc
RUN chmod 0644 /etc/zsh/zshrc
# a list of zsh plugins that will be installed by antibody
COPY src/etc/zsh/antibody-plugins.txt /etc/zsh/antibody-plugins.txt
RUN chmod 0644 /etc/zsh/antibody-plugins.txt
# a bunch of useful zsh aliases
COPY src/etc/zsh/plugins/ /etc/zsh/plugins/

# add some aliases for all users (and all shells, although this
# file is by convention only meant for bash)
COPY src/etc/profile.d/alias.sh /etc/profile.d/alias.sh

# install vim-plug globally for neovim
RUN curl -fLo /usr/share/nvim/runtime/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# copy initial neovim sysinit.vim in the container
COPY src/usr/share/nvim/sysinit.vim /usr/share/nvim/sysinit.vim
RUN chmod 0644 /usr/share/nvim/sysinit.vim

# prevent neovim from whining about some spelling libs
RUN ln -s /usr/share/vim/vim80/spell /usr/share/nvim/runtime/spell

# make sure the lib exists with a default name
RUN ln -s /usr/lib/x86_64-linux-gnu/libmysqlclient.so.20 /usr/lib/x86_64-linux-gnu/libmysqlclient.so

# add sqlines
# http://www.sqlines.com/
RUN SOV_DS_SQLINES_DATA_FNAME=sqlinesdata31773_x86_64_linux.tar.gz \
    && SOV_DS_SQLINES_CONV_FNAME=sqlines31107_linux.tar.gz \
    && SOV_DS_SQLINES_DATA_URL=http://www.sqlines.com/downloads/"${SOV_DS_SQLINES_DATA_FNAME}" \
    && SOV_DS_SQLINES_CONV_URL=http://www.sqlines.com/downloads/"${SOV_DS_SQLINES_CONV_FNAME}" \
    && SOV_DS_SQLINES_PATH=/opt/sqlines \
    && mkdir -p  ${SOV_DS_SQLINES_PATH} \
    && wget ${SOV_DS_SQLINES_DATA_URL} \
    && tar xvf ${SOV_DS_SQLINES_DATA_FNAME} -C ${SOV_DS_SQLINES_PATH} --strip-components 1 \
    && rm -rf ${SOV_DS_SQLINES_DATA_FNAME} \
    && wget ${SOV_DS_SQLINES_CONV_URL} \
    && tar xvf ${SOV_DS_SQLINES_CONV_FNAME} -C ${SOV_DS_SQLINES_PATH} --strip-components 1 \
    && rm -rf ${SOV_DS_SQLINES_CONV_FNAME} \
    && unset SOV_DS_SQLINES_DATA_FNAME \
    && unset SOV_DS_SQLINES_CONV_FNAME \
    && unset SOV_DS_SQLINES_DATA_URL \
    && unset SOV_DS_SQLINES_CONV_URL

# fix some permissions that git didn't want to transmit
RUN chmod go+r /etc/zsh/zshrc /etc/zsh/antibody-plugins.txt /etc/zsh/plugins/* /etc/profile.d/* /etc/tmux.conf

# open JupyterHub's proxy's port to the outside
EXPOSE 8000
# SSH port
EXPOSE 22
# Flow UI Port
EXPOSE 5000

# install statusbar for jupyterlab
RUN jupyter labextension install @jupyterlab/statusbar

# install bat, a cat replacement with syntax highlighting
RUN wget https://github.com/sharkdp/bat/releases/download/v0.8.0/bat_0.8.0_amd64.deb \
    && dpkg -i bat_0.8.0_amd64.deb

# copy flow library into image
COPY *.whl /app/
# and install it
RUN pip install *.whl

# copy the start script over
COPY src/app/run /app/run

# download and install pycharm
RUN wget https://download.jetbrains.com/python/pycharm-community-2018.3.tar.gz \
    && tar xfz pycharm-*.tar.gz -C /opt/ \
    && rm pycharm-*.tar.gz
# copy global xfce menu entries (pycharm)
COPY src/usr/share/applications/pycharm-ce.desktop /usr/share/applications/pycharm-ce.desktop

# copy global git configs
COPY src/etc/gitconfig /etc/gitconfig
RUN chmod 0644 /etc/gitconfig

# copy desktop entry files for autostart
#COPY src/etc/xdg/autostart/cairo-dock.desktop /etc/xdg/autostart/cairo-dock.desktop

# run this when the container launches
CMD ["./run"]
